package com.etong.android.frame.publisher;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.etong.android.frame.utils.logger.Logger;

import org.simple.eventbus.EventBus;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody.Builder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author dengzhe Created on 2019/2/1 14:42
 * E-mail:sandaohaizi@gmail.com
 * Alarm: 未经同意，严禁修改
 * desc : HttpPublisher
 */


public class HttpPublisher extends Publisher {
    public static final int NETWORK_ERROR = 4353;
    public static final int HTTP_ERROR = 4369;
    /**
     * @deprecated
     */
    @Deprecated
    public static final int VOLLEY_ERROR = 4369;
    private Context mContext;
    private OkHttpClient client;
    private Map<String, String> mHttpToken;
    private static final String DEFAULT_TOKEN_NAME = "accessToken";
    private static final String TAG = "HttpPubliser";

    private HttpPublisher() {
        this.mContext = null;
        this.client = null;
        this.mHttpToken = new HashMap();
    }

    public static final HttpPublisher getInstance() {
        return HttpPublisher.Holder.INSTANCE;
    }

    public void initialize(Context context, OkHttpClient okHttpClient) {
        this.mContext = context.getApplicationContext();
        this.client = okHttpClient;
    }

    public void setToken(String token) {
        if (null != token && !token.isEmpty()) {
            this.mHttpToken.put("accessToken", token);
        }

    }

    public void setToken(String name, String token) {
        if (null != name && null != token && !token.isEmpty() && !name.isEmpty()) {
            this.mHttpToken.put(name, token);
        }

    }

    public boolean checkNetworkState() {
        @SuppressLint("WrongConstant") ConnectivityManager connectivity = (ConnectivityManager) this.mContext.getSystemService("connectivity");
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null && info.isConnected() && info.getState() == State.CONNECTED) {
                return true;
            }
        }

        return false;
    }

    public void sendRequest(HttpMethod method, String tag, String flag) {
        if (!this.checkNetworkState()) {
            JSONObject data = new JSONObject();
            data.put("errCode", 4353);
            data.put("errName", "网络异常");
            method.put(data);
            Logger.e(method.getParam().toString(), new Object[0]);
            Logger.json(data.toJSONString());
            this.getEventBus().post(method, "http_error");
            this.getEventBus().post(method, tag);
        } else {
            Builder formBuilder = new Builder();
            Map<String, Object> map = method.getParam();
            if (map != null && !map.isEmpty()) {
                Iterator var5 = map.keySet().iterator();

                while (var5.hasNext()) {
                    String key = (String) var5.next();
                    formBuilder.add(key, method.getParam().get(key).toString());
                }
            }
            Request request = null;
            if (flag.equals("get")) {
                request = (new Request.Builder()).url(method.getUrl()).get().tag(tag).build();
            } else {
                request = (new Request.Builder()).url(method.getUrl()).post(formBuilder.build()).tag(tag).build();
            }

            this.client.newCall(request).enqueue(new HttpPublisher.JsonObjectCallback(tag, method));
        }
    }

    public void sendRequestWithToken(HttpMethod method, String tag) {
        Iterator var3 = this.mHttpToken.keySet().iterator();

        while (var3.hasNext()) {
            String key = (String) var3.next();
            method.getParam().put(key, this.mHttpToken.get(key));
        }

        this.sendRequest(method, tag, "");
    }

    public void cancelAll() {
        Iterator var1 = this.client.dispatcher().queuedCalls().iterator();

        Call call;
        while (var1.hasNext()) {
            call = (Call) var1.next();
            call.cancel();
        }

        var1 = this.client.dispatcher().runningCalls().iterator();

        while (var1.hasNext()) {
            call = (Call) var1.next();
            call.cancel();
        }

    }

    public void cancel(String tag) {
        if (!TextUtils.isEmpty(tag)) {
            Iterator var2 = this.client.dispatcher().queuedCalls().iterator();

            Call call;
            while (var2.hasNext()) {
                call = (Call) var2.next();
                if (tag.equals(call.request().tag())) {
                    call.cancel();
                }
            }

            var2 = this.client.dispatcher().runningCalls().iterator();

            while (var2.hasNext()) {
                call = (Call) var2.next();
                if (tag.equals(call.request().tag())) {
                    call.cancel();
                }
            }

        }
    }

    private static class Holder {
        private static final HttpPublisher INSTANCE = new HttpPublisher();

        private Holder() {
        }
    }

    public class JsonObjectCallback implements Callback {
        private HttpMethod method;
        private String eventTag;

        public JsonObjectCallback(String tag, HttpMethod method) {
            this.eventTag = tag;
            this.method = method;
        }

        public void onFailure(Call call, IOException e) {
            JSONObject data = new JSONObject();
            data.put("errCode", 4369);
            if (call.isCanceled()) {
                data.put("errName", "Http访问被取消");
            } else {
                data.put("errName", "Http访问异常");
            }

            this.method.put(data);
            Logger.e(e, "HttpPublisher onFailure", new Object[0]);
            if (this.method.getParam() != null && !this.method.getParam().isEmpty()) {
                Logger.e(this.method.getParam().toString(), new Object[0]);
            }

            Logger.json(this.method.data().toJSONString());
            EventBus.getDefault().post(this.method, this.eventTag);
        }

        public void onResponse(Call call, Response response) {
            JSONObject data = null;
            String str = null;

            try {
                str = response.body().string();
                data = JSON.parseObject(str);
            } catch (IOException var6) {
                data = new JSONObject();
                data.put("errCode", 4369);
                data.put("errName", "Http访问异常,返回数据异常");
                var6.printStackTrace();
            } catch (JSONException var7) {
                data = new JSONObject();
                data.put("data", str);
                data.put("errCode", 4369);
                data.put("errName", "Http访问异常,返回数据异常");
                var7.printStackTrace();
            }

            EventBus.getDefault().post(this.method.put(data), this.eventTag);
            if (this.method.getParam() != null) {
                Logger.d(this.method.getParam().toString(), new Object[0]);
            }
            if (this.method.data() != null) {
                Logger.json(this.method.data().toJSONString());
            }
        }
    }
}
